﻿Public Class Island

    Public Property IslandName As String
    Public Property IslandLeader As String
    Public Property IslandCapital As String
    Public Property PopCounts As Integer()
    Public Property TotalPop As Integer

    Public Property IslandSize As IslSize
    Public Property ProdomRace As Race

    Public Property PopCentreList As List(Of PopCentre)
    Public Property PoiList As List(Of PPoi)

    Private Property BList As List(Of String)

    Public Sub New(NewName As String)
        Dim LargestPop As PopCentre = New PopCentre()
        Dim nPop As PopCentre

        IslandName = NewName
        IslandSize = IslSize.Large
        ProdomRace = Race.Human
        PopCounts = GetLocs(IslandSize, True)

        PopCentreList = New List(Of PopCentre)

        BList = New List(Of String)

        For Each l As String In IO.File.ReadAllText("pl.txt").Split(":")
            BList.Add(l)
        Next

        For C = 0 To PopCounts(0) - 1
            nPop = New PopCentre(LocType.City, ProdomRace, C, GetLoc)
            PopCentreList.Add(nPop)

            If nPop.LocationPopulation > LargestPop.LocationPopulation Then
                LargestPop = nPop
            End If
        Next

        For T = 0 To PopCounts(1) - 1
            nPop = New PopCentre(LocType.Town, ProdomRace, T + 100, GetLoc)
            PopCentreList.Add(nPop)

            If nPop.LocationPopulation > LargestPop.LocationPopulation Then
                LargestPop = nPop
            End If
        Next

        For V = 0 To PopCounts(2) - 1
            nPop = New PopCentre(LocType.Village, ProdomRace, V + 1000, GetLoc)
            PopCentreList.Add(nPop)

            If nPop.LocationPopulation > LargestPop.LocationPopulation Then
                LargestPop = nPop
            End If
        Next

        IslandLeader = LargestPop.LocationLeader
        IslandCapital = LargestPop.LocationName
    End Sub

    Public Sub New(NewName As String, NewSize As IslSize, RandomiseRace As Boolean, posList As List(Of String), frm As Form1, Optional NewRace As Race = Race.Human)
        Dim nRan As New Random
        Dim LargestPop As PopCentre = New PopCentre()
        Dim nPop As PopCentre
        Dim frmOText As String = frm.lbl_prog.Text
        Dim Royalty As String = ""

        BList = New List(Of String)

        IslandName = NewName
        IslandSize = NewSize

        For Each p As String In posList
            BList.Add(p)
        Next

        If RandomiseRace Then
            If nRan.Next(1000000) > 500000 Then
                ProdomRace = Race.Elf
            Else
                ProdomRace = Race.Human
            End If
        Else
            ProdomRace = NewRace
        End If

        UDateText(frmOText & " - Main Race: " & ProdomRace)
        CountSteps()

        PopCounts = GetLocs(IslandSize)

        PopCentreList = New List(Of PopCentre)

        UDateText(frmOText & " - Set Pop Counts : Cities ")
        CountSteps()

        For C = 0 To PopCounts(0) - 1
            nPop = New PopCentre(LocType.City, ProdomRace, C, GetLoc)
            PopCentreList.Add(nPop)

            If nPop.LocationPopulation > LargestPop.LocationPopulation Then
                LargestPop = nPop
            End If
        Next

        UDateText(frmOText & " - Set Pop Counts : Towns")
        CountSteps()


        For T = 0 To PopCounts(1) - 1

            nPop = New PopCentre(LocType.Town, ProdomRace, T * 100, GetLoc)
            PopCentreList.Add(nPop)

            If nPop.LocationPopulation > LargestPop.LocationPopulation Then
                LargestPop = nPop
            End If
        Next

        UDateText(frmOText & " - Set Pop Counts : Villages")
        CountSteps()

        For V = 0 To PopCounts(2) - 1

            nPop = New PopCentre(LocType.Village, ProdomRace, V + 1000, GetLoc)
            PopCentreList.Add(nPop)

            If nPop.LocationPopulation > LargestPop.LocationPopulation Then
                LargestPop = nPop
            End If
        Next

        For Each p As PopCentre In PopCentreList
            TotalPop += p.LocationPopulation
        Next

        UDateText(frmOText & " - Total Population: " & TotalPop)
        CountSteps()

        If Not IslandSize = IslSize.Small Then
            Royalty = GetTitle(GetGender(TotalPop), ProdomRace, True) & " " & GetName(GetGender(TotalPop), ProdomRace, TotalPop + LargestPop.LocationPopulation)
        End If

        Select Case IslandSize
            Case IslSize.Large
                If New Random(TotalPop).Next(1000000) > 750000 Then
                    IslandLeader = LargestPop.LocationLeader
                Else
                    IslandLeader = Royalty
                End If
            Case IslSize.Medium
                If New Random(TotalPop).Next(1000000) > 400000 Then
                    IslandLeader = LargestPop.LocationLeader
                Else
                    IslandLeader = Royalty
                End If
            Case IslSize.Small
                IslandLeader = LargestPop.LocationLeader
            Case Else
                IslandLeader = "Error McGee"
        End Select

        UDateText(frmOText & " - Leaders Done")
        CountSteps()

        IslandCapital = LargestPop.LocationName

        UDateText(frmOText & " - Points of Intrest ")
        CountSteps()

        GetPois()

        UDateText(frmOText & " - Done ")
        CountSteps()
    End Sub

    Public Sub Save()
        Dim oList As New List(Of String) From {
            "Name: " & IslandName,
            "Size: " & IslandSize.ToString,
            "Main Race: " & ProdomRace.ToString,
            "Main Leader: " & IslandLeader,
            "Capital: " & IslandCapital,
            "Cities: " & PopCounts(0) & ", Towns: " & PopCounts(1) & ", Villages: " & PopCounts(2),
            "Total Population: " & TotalPop,
            "Population Centres; "
        }

        For Each pc As PopCentre In PopCentreList
            oList.Add(vbTab & pc.LocationType.ToString & " - " & pc.LocationName & "(" & pc.LocationPopulation & ") : " & pc.LocationLeader & " - Location : " & pc.LocationCoords)
        Next

        If Not IO.Directory.Exists("Output") Then
            IO.Directory.CreateDirectory("Output")
        End If

        IO.File.WriteAllLines("Output/" & IslandName & ".txt", oList.ToArray)
    End Sub

    Public Function SaveData() As String
        Dim Result As String = IslandName & "," & IslandSize.ToString & "," & ProdomRace.ToString & "," & IslandLeader & "," & IslandCapital & "," & PopCounts(0) & "." & PopCounts(1) & "." & PopCounts(2) & "," & TotalPop & ","

        For Each pc As PopCentre In PopCentreList
            Result &= pc.LocationType.ToString & ":" & pc.LocationName & ":" & pc.LocationPopulation & ":" & pc.LocationLeader & ":" & pc.LocationCoords & "|"
        Next

        Result = Result.TrimEnd("|")

        Return Result
    End Function


    Private Function GetLoc() As String
        Dim Result As String
        Dim r As Integer = GetR(0, BList.Count - 1)

        Result = BList(r)
        BList.RemoveAt(r)

        Return Result
    End Function

    Private Sub GetPois()
        Dim nPoi As PPoi

        PoiList = New List(Of PPoi)

        For i = 0 To PoiCount(IslandSize)
            nPoi = New PPoi(GetRandPoi(), GetLoc())
            PoiList.Add(nPoi)
        Next

    End Sub

End Class
