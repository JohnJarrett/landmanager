﻿Public Module Draw_Stuff

    Const FillRect As Boolean = True

    Private Function DrawMap(bimg As String, isle As Island) As Image
        Dim result As Image
        Dim g As Graphics
        Dim x, y, c, bcx, bcy As Integer
        Dim s As Integer = 5

        Dim F As Font = New Font("Calibri", 9, FontStyle.Regular)
        Dim SF As Font = New Font("Arial", 9, FontStyle.Regular)

        Dim i As Bitmap

        i = Image.FromFile(bimg)

        g = Graphics.FromImage(i)

        result = New Bitmap(i.Width, i.Height)

        ' Dim cc As Integer

        'For xxx = 0 To i.Width - 1 Step 16
        '    g.DrawLine(Pens.Black, New Point(xxx, 0), New Point(xxx, i.Width))
        'Next

        'For yyy = 0 To i.Height - 1 Step 16
        '    g.DrawLine(Pens.Black, New Point(0, yyy), New Point(i.Height, yyy))
        'Next

        'For Each p As PPoi In isle.PoiList
        '    x = p.Loc.Split(",")(0)
        '    y = p.Loc.Split(",")(1)

        '    g.TextRenderingHint = Text.TextRenderingHint.AntiAlias
        '    g.DrawString(p.Poi.Symb, F, New SolidBrush(p.Poi.Colour), x, y)
        'Next

        For Each l As PopCentre In isle.PopCentreList
            x = l.LocationCoords.Split(",")(0)
            y = l.LocationCoords.Split(",")(1)
            c += 1

            bcx = x
            bcy = y

            While bcx > 16
                bcx -= 16
            End While

            While bcy > 16
                bcy -= 16
            End While

            bcx = ((x - bcx) + 8) - (g.MeasureString(c, F).Width / 2)
            bcy = ((y - bcy) + 8) - (g.MeasureString(c, F).Height / 2)

            g.FillEllipse(Brushes.Red, New Rectangle(x - (s / 2), y - (s / 2), s, s))
            g.TextRenderingHint = Text.TextRenderingHint.AntiAlias
            g.DrawString(c, F, Brushes.Black, bcx, bcy)
        Next

        For Each l As PPoi In isle.PoiList
            x = l.Loc.Split(",")(0)
            y = l.Loc.Split(",")(1)
            'cc += 1

            'bcx = x
            'bcy = y

            'While bcx > 16
            '    bcx -= 16
            'End While

            'While bcy > 16
            '    bcy -= 16
            'End While

            'bcx = ((x - bcx) + 8) - (g.MeasureString(c, F).Width / 2)
            'bcy = ((y - bcy) + 8) - (g.MeasureString(c, F).Height / 2)

            g.FillEllipse(New SolidBrush(l.Poi.Colour), New Rectangle(x - (s / 2), y - (s / 2), s, s))
            'g.TextRenderingHint = Text.TextRenderingHint.AntiAlias
            'g.DrawString(l.Poi.NextID & cc, F, Brushes.Black, bcx, bcy)
        Next

        g = Graphics.FromImage(result)

        If FillRect Then
            g.FillRectangle(Brushes.White, 0, 0, i.Width, i.Height)
        End If

        g.DrawImage(i, 0, 0, 512, 512)

        Return result
    End Function

    Private Function DrawInfo(isle As Island) As Image
        Dim result As Image
        Dim g As Graphics

        Dim MF As Font = New Font("Calibri", 30, FontStyle.Regular) ' Main Font
        Dim SF As Font = New Font("Monotype Corsiva", 20, FontStyle.Regular) ' Subtitle Font
        Dim TF As Font = New Font("Monotype Corsiva", 60, FontStyle.Regular) ' Title Font
        Dim RF As Font = New Font("Calibri", 21, FontStyle.Regular) ' Race Font

        Dim tX, tY, stvs, ttvs As Integer

        Dim rlist As New List(Of String)
        Dim txtw, txth As String

        result = New Bitmap(512, 512)

        stvs = 32
        ttvs = 42

        g = Graphics.FromImage(result)

        g.TextRenderingHint = Text.TextRenderingHint.AntiAlias

        ' ========== Name

        tY = g.MeasureString(isle.IslandName, TF).Width
        tX = 16

        If FillRect Then
            g.FillRectangle(Brushes.White, 0, 0, 512, 512)
        End If

        g.DrawString(isle.IslandName, TF, Brushes.Black, (512 / 2) - (tY / 2), tX)

        ' ========== Nation

        txth = isle.IslandName
        txtw = isle.IslandSize.ToString & " " & isle.ProdomRace.ToString & " Nation"

        tY = g.MeasureString(txtw, RF).Width
        tX += g.MeasureString(txth, TF).Height - 20

        g.DrawString(txtw, RF, Brushes.Black, (512 / 2) - (tY / 2), tX)

        ' ========== Capital

        txth = txtw
        txtw = "Capital"

        tY = g.MeasureString(txtw, SF).Width
        tX += g.MeasureString(txth, RF).Height + 64

        g.DrawString(txtw, SF, Brushes.Black, (512 / 2) - (tY / 2), tX - stvs)

        txth = txtw
        txtw = isle.IslandCapital

        tY = g.MeasureString(txtw, MF).Width
        tX += g.MeasureString(txth, SF).Height

        g.DrawString(txtw, MF, Brushes.Black, (512 / 2) - (tY / 2), tX - ttvs)

        ' ========== Ruler

        txth = txtw
        txtw = "Ruler"

        tY = g.MeasureString(txtw, SF).Width
        tX += g.MeasureString(txth, MF).Height + 16

        g.DrawString(txtw, SF, Brushes.Black, (512 / 2) - (tY / 2), tX - stvs)

        txth = txtw
        txtw = isle.IslandLeader

        tY = g.MeasureString(txtw, MF).Width
        tX += g.MeasureString(txth, SF).Height

        g.DrawString(txtw, MF, Brushes.Black, (512 / 2) - (tY / 2), tX - ttvs)

        ' ========== Population

        txth = txtw
        txtw = "Population"

        tY = g.MeasureString(txtw, SF).Width
        tX += g.MeasureString(txth, MF).Height + 16

        g.DrawString(txtw, SF, Brushes.Black, (512 / 2) - (tY / 2), tX - stvs)

        txth = txtw
        txtw = isle.TotalPop

        tY = g.MeasureString(txtw, MF).Width
        tX += g.MeasureString(txth, SF).Height

        g.DrawString(txtw, MF, Brushes.Black, (512 / 2) - (tY / 2), tX - ttvs)
        Return result
    End Function

    Private Function DrawPopKey(isle As Island) As Image
        Dim result As Image
        Dim g As Graphics
        Dim c, cc, sw, sh, tY As Integer
        Dim ColC As Integer = 20

        Dim li As New List(Of Image)
        Dim i As Image

        Dim lngstring As Single
        Dim str As String

        Dim FS As Font = New Font("Calibri", 11, FontStyle.Regular)

        Dim plist As New List(Of List(Of String))
        Dim rlist As New List(Of String)

        g = Graphics.FromImage(New Bitmap(512, 512))

        For Each l As PopCentre In isle.PopCentreList
            c += 1
            cc += 1
            str = LeadZero(c) & " (" & l.LocationType.ToString.Substring(0, 1) & ") - " & l.LocationName & " (" & l.LocationPopulation & ")" & " : " & l.LocationLeader
            sh = g.MeasureString(str, FS).Height - 4

            If cc > ColC Then
                plist.Add(rlist)
                rlist = New List(Of String)
                cc = 1
            End If

            sw = g.MeasureString(str, FS).Width

            If sw > lngstring Then
                lngstring = sw
            End If

            rlist.Add(str)
        Next

        plist.Add(rlist)

        For Each lst As List(Of String) In plist
            i = New Bitmap(lngstring, sh * ColC + 12, Imaging.PixelFormat.Format32bppArgb)

            g = Graphics.FromImage(i)

            g.TextRenderingHint = Text.TextRenderingHint.AntiAlias

            tY = 0

            For Each popLoc As String In lst
                g.DrawString(popLoc, FS, Brushes.Black, 0, tY)
                tY += sh
            Next

            li.Add(i)
        Next

        result = New Bitmap(lngstring * li.Count, sh * ColC, Imaging.PixelFormat.Format32bppArgb)

        g = Graphics.FromImage(result)

        If FillRect Then
            g.FillRectangle(Brushes.White, 0, 0, result.Width, result.Height)
        End If

        cc = 0

        For Each img As Image In li
            g.DrawImage(img, lngstring * cc, 0, lngstring, sh * ColC)
            cc += 1
        Next

        Return result
    End Function

    Public Function DrawMapKey() As Image
        Dim g As Graphics
        Dim Result As Image
        Dim h, w, x, y, mxlng, mxhei As Single
        Dim Types As New List(Of String)
        Dim Cols As New List(Of Color)
        Dim f As New Font("Arial", 6, FontStyle.Bold)
        Dim doTest As Boolean = False

        Result = New Bitmap(128, 128)
        g = Graphics.FromImage(Result)

        g.TextRenderingHint = Text.TextRenderingHint.AntiAlias

        For Each po As POIs In APoiT
            Types.Add(po.Name)
            Cols.Add(po.Colour)
            If g.MeasureString(po.Name, f).Width > mxlng Then
                mxlng = g.MeasureString(po.Name, f).Width
            End If
        Next

        mxhei = g.MeasureString("OOO", f).Height + 8

        w = mxlng + 4
        h = (mxhei + 8) * Types.Count

        Result = New Bitmap(w, h, Imaging.PixelFormat.Format32bppArgb)

        g = Graphics.FromImage(Result)

        If FillRect Then
            g.FillRectangle(Brushes.Yellow, 0, 0, Result.Width, Result.Height)
        End If

        g.TextRenderingHint = Text.TextRenderingHint.AntiAlias

        y = -mxhei + 8

        For i = 0 To Types.Count - 1
            x = (w / 2) - (g.MeasureString(Types(i), f).Width / 2)
            y += mxhei + 8
            g.DrawString(Types(i), f, Brushes.Black, x, y)
            g.FillRectangle(New SolidBrush(Cols(i)), w / 2, y - 8, 5, 5)
        Next

        If doTest Then
            Dim ntes As New ImgTester(Result)
            ntes.Show()
            MessageBox.Show(Result.Height & " = H - W = " & Result.Width)
        End If

        Return Result
    End Function

    Private Function DrawImage(bimg As String, Isle As Island) As Image
        Dim mi, ii, ki, top, mk, result As Image
        Dim g As Graphics
        Dim gap As Single = 116.75
        Dim gap2 As Single = -6
        Dim w, h, jw, kw As Single

        mi = DrawMap(bimg, Isle)
        ii = DrawInfo(Isle)
        ki = DrawPopKey(Isle)
        mk = DrawMapKey()

        jw = mi.Width + (gap - mk.Width) + ii.Width + mk.Width
        kw = ki.Width

        If jw > kw Then
            w = jw
        Else
            w = kw
        End If

        h = mi.Height + gap2 + ki.Height

        result = New Bitmap(w, h, Imaging.PixelFormat.Format32bppArgb)

        top = New Bitmap(jw, mi.Height, Imaging.PixelFormat.Format32bppArgb)

        g = Graphics.FromImage(top)

        g.DrawImage(mi, 0, 0) ', mi.Width, mi.Height)

        w = mi.Width + (((gap - mk.Width) / 2) - (mk.Width / 2))
        g.DrawImage(mk, w, 0)

        g.DrawImage(ii, (mi.Width + (gap - mk.Width)), 0) ', ii.Width, ii.Height)

        g = Graphics.FromImage(result)

        w = (result.Width / 2) - (top.Width / 2)

        g.DrawImage(top, w, 0)

        w = (result.Width / 2) - (ki.Width / 2)

        g.DrawImage(ki, w, mi.Height + gap2) ', ki.Width, ki.Height)
        Return result
    End Function

    Public Function GetMap(bImg As String, Isle As Island) As Image
        Dim Result As Image
        Dim g As Graphics
        Dim x, y As Single
        Dim mI As Image = DrawImage(bImg, Isle)

        Result = Image.FromFile("Page.png")

        g = Graphics.FromImage(Result)

        x = (Result.Width / 2) - (mI.Width / 2)
        y = (Result.Height / 2) - (mI.Height / 2)

        g.DrawImage(mI, x, y)

        Return Result
    End Function

    Private Function LeadZero(num As Integer) As String
        Dim result As String

        Select Case num
            Case < 10
                result = "0" & num
            Case Else
                result = num
        End Select

        Return result
    End Function
End Module
