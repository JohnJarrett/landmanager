﻿Public Class ImgTester
    Public Sub New(img As Image)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        PictureBox1.Image = img
        Width = 64 + img.Width
        Height = 64 + img.Height
    End Sub
End Class