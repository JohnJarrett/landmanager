﻿Public Class PopCentre
    Public Property LocationName As String
    Public Property LocationType As LocType
    Public Property LocationPopulation As Integer
    Public Property LocationLeader As String = "NA"
    Public Property LocationCoords As String

    Public Sub New()
        LocationName = ""
        LocationType = LocType.Village
        LocationCoords = "0,0"
    End Sub

    Public Sub New(L As LocType, R As Race, S As Integer, lc As String, Optional NoTitle As Boolean = False)
        Dim LeadGender As Gender
        Dim Title As String

        LocationType = L

        Select Case L
            Case LocType.City
                LocationPopulation = GetPop(LocType.City, S)
            Case LocType.Town
                LocationPopulation = GetPop(LocType.Town, S)
            Case LocType.Village
                LocationPopulation = GetPop(LocType.Village, S)
        End Select

        LeadGender = GetGender(S)

        If NoTitle Then
            Title = ""
        Else
            Title = GetTitle(LeadGender, R, False) & " "
        End If


        LocationName = GetName(Gender.Town, R, LocationPopulation + S)
        LocationLeader = Title & GetName(LeadGender, R, S)

        LocationCoords = lc
    End Sub

End Class
