﻿Public Module Namer

    Private ReadOnly UsedNames As New List(Of String)
    Private ReadOnly UsedTownNames As New List(Of String)
    Private MyForm As Form1

    Private LocalUsedTownNames As List(Of String)

    Const MaxTries As Integer = 5000
    Private ReadOnly nr As New Random

    Public Enum Gender
        Male = 0
        Female = 1
        Town = 2
    End Enum

    Public Enum Race
        Human = 0
        Elf = 1
    End Enum

    Private HMList, HFList, HLAList, HLBList, HTAList, HTBList As List(Of String)
    Private EMList, EFList, ELList, ETAList, ETBList As List(Of String)

    Public Sub ClearLocal()
        LocalUsedTownNames = New List(Of String)
    End Sub

    Public Sub SetForm(frm As Form1)
        MyForm = frm
    End Sub

    Public Sub UDateText(NewString As String)
        MyForm.UT(NewString)
    End Sub

    Public Sub CountSteps()
        MyForm.UpdateProgress()
    End Sub

    Public Sub FillLists()
        HMList = New List(Of String)
        HFList = New List(Of String)
        HLAList = New List(Of String)
        HLBList = New List(Of String)
        HTAList = New List(Of String)
        HTBList = New List(Of String)

        EMList = New List(Of String)
        EFList = New List(Of String)
        ELList = New List(Of String)
        ETAList = New List(Of String)
        ETBList = New List(Of String)

        For Each n As String In IO.File.ReadAllLines("files/NamesHM.txt") ' Human M
            HMList.Add(n)
        Next

        For Each n As String In IO.File.ReadAllLines("files/NamesHF.txt") ' Human F
            HFList.Add(n)
        Next

        For Each n As String In IO.File.ReadAllLines("files/NamesHLA.txt") ' Human Last A
            HLAList.Add(n)
        Next

        For Each n As String In IO.File.ReadAllLines("files/NamesHLB.txt") ' Human Last B
            HLBList.Add(n)
        Next

        For Each n As String In IO.File.ReadAllLines("files/TownsHA.txt") ' Human Town A
            HTAList.Add(n)
        Next

        For Each n As String In IO.File.ReadAllLines("files/TownsHB.txt") ' Human Town B
            HTBList.Add(n)
        Next

        For Each n As String In IO.File.ReadAllLines("files/NamesEM.txt") ' Elf M
            EMList.Add(n)
        Next

        For Each n As String In IO.File.ReadAllLines("files/NamesEF.txt") ' Elf F
            EFList.Add(n)
        Next

        For Each n As String In IO.File.ReadAllLines("files/NamesEL.txt") ' Elf Last
            ELList.Add(n)
        Next

        For Each n As String In IO.File.ReadAllLines("files/TownsEA.txt") ' Elf Town A
            ETAList.Add(n)
        Next

        For Each n As String In IO.File.ReadAllLines("files/TownsEB.txt") ' Elf Town B
            ETBList.Add(n)
        Next
    End Sub

    Public Function GetName(G As Gender, R As Race, S As Integer)
        Dim Result As String = "ERROR"
        Dim lName As String
        Dim ofcount As Integer = 0

        Select Case G
            Case Gender.Male
                Select Case R
                    Case Race.Human
                        lName = HMList(GRN(HMList, S))
                        Result = lName & " " & GetHLastName(S + 2)

                        If UsedNames.Contains(Result) Then
                            While UsedNames.Contains(Result)
                                ofcount += 1
                                If ofcount = MaxTries Then
                                    Result &= " I"
                                    Exit While
                                End If

                                Result = lName & " " & GetHLastName(StrToSeed(Result))
                            End While
                        End If

                        UsedNames.Add(Result)

                    Case Race.Elf
                        lName = EMList(GRN(ELList, S))
                        Result = lName & " " & ELList(GRN(ELList, S + 2))

                        If UsedNames.Contains(Result) Then
                            While UsedNames.Contains(Result)
                                ofcount += 1
                                If ofcount = MaxTries Then
                                    Result &= " I"
                                    Exit While
                                End If

                                Result = lName & " " & ELList(GRN(ELList, StrToSeed(Result)))
                            End While
                        End If

                        UsedNames.Add(Result)

                End Select
            Case Gender.Female
                Select Case R
                    Case Race.Human
                        lName = HFList(GRN(HFList, S))
                        Result = lName & " " & GetHLastName(S + 2)

                        If UsedNames.Contains(Result) Then
                            While UsedNames.Contains(Result)
                                ofcount += 1
                                If ofcount = MaxTries Then
                                    Result &= " I"
                                    Exit While
                                End If

                                Result = lName & " " & GetHLastName(StrToSeed(Result))
                            End While
                        End If

                        UsedNames.Add(Result)

                    Case Race.Elf
                        lName = EFList(GRN(EFList, S))
                        Result = lName & " " & ELList(GRN(ELList, S + 2))

                        If UsedNames.Contains(Result) Then
                            While UsedNames.Contains(Result)
                                ofcount += 1
                                If ofcount = MaxTries Then
                                    Result &= " I"
                                    Exit While
                                End If

                                Result = lName & " " & ELList(GRN(ELList, StrToSeed(Result)))
                            End While
                        End If

                        UsedNames.Add(Result)

                End Select
            Case Gender.Town

                Dim tnState = -1
                Dim oName As String

                Select Case R
                    Case Race.Human
                        Result = GetHTownName(S)
                        oName = Result
                        If UsedTownNames.Contains(Result) Then
                            While UsedTownNames.Contains(Result)
                                ofcount += 1

                                If Not LocalUsedTownNames.Contains(Result) Then
                                    If GetR(0, 50000) > 25000 Then
                                        ofcount = MaxTries
                                    End If
                                End If

                                If ofcount = MaxTries Then
                                    Exit While
                                End If

                                If tnState = -1 Then
                                    If GetR(0, 50000) < 25000 Then
                                        Result = "Old " & oName
                                    End If
                                    tnState = 0
                                ElseIf tnState = 0 Then
                                    Result = "New " & oName
                                    tnState = 1
                                ElseIf tnState = 1 Then
                                    Result = GetHTownName(StrToSeed(Result))
                                    oName = Result
                                    tnState = -1
                                End If
                            End While
                        End If

                    Case Race.Elf
                        Result = GetETownName(S)
                        oName = Result
                        If UsedTownNames.Contains(Result) Then
                            While UsedTownNames.Contains(Result)
                                ofcount += 1

                                If Not LocalUsedTownNames.Contains(Result) Then
                                    If GetR(0, 50000) < 25000 Then
                                        ofcount = MaxTries
                                    End If
                                End If

                                If ofcount = MaxTries Then
                                    Exit While
                                End If

                                If tnState = -1 Then
                                    If GetR(0, 50000) > 25000 Then
                                        Result = "Os " & oName
                                    End If

                                    tnState = 0
                                ElseIf tnState = 0 Then
                                    Result = "Nes " & oName
                                    tnState = 1
                                ElseIf tnState = 1 Then
                                    Result = GetETownName(StrToSeed(Result))
                                    oName = Result
                                    tnState = -1
                                End If
                            End While
                        End If
                End Select

                LocalUsedTownNames.Add(Result)
                UsedTownNames.Add(Result)
        End Select

        Return Result
    End Function

    Private Function GetHLastName(S As Integer) As String
        Dim result As String
        Dim ofcount As Integer = 0

        result = HLAList(GRN(HLAList, S)) & HLBList(GRN(HLBList, S + 2))

        If UsedNames.Contains(result) Then
            While UsedNames.Contains(result)
                ofcount += 1
                If ofcount = MaxTries Then
                    Exit While
                End If

                result = HLAList(GRN(HLAList, StrToSeed(result))) & HLBList(GRN(HLBList, StrToSeed(result)))
            End While
        End If

        UsedNames.Add(result)

        Return result
    End Function

    Private Function GetHTownName(S As Integer) As String
        Dim result As String = HTAList(GRN(HTAList, S)) & HTBList(GRN(HTBList, S + 2))
        Dim ofcount As Integer = 0

        If UsedNames.Contains(result) Then
            While UsedNames.Contains(result)
                ofcount += 1
                If ofcount = MaxTries Then
                    Exit While
                End If

                result = HTAList(GRN(HTAList, StrToSeed(result))) & HTBList(GRN(HTBList, StrToSeed(result)))
            End While
        End If

        UsedNames.Add(result)

        Return result
    End Function

    Public Function GetETownName(S As Integer) As String
        Dim result As String = ETAList(GRN(ETAList, S)) & ETBList(GRN(ETBList, S + 2))
        Dim ofcount As Integer = 0

        If UsedNames.Contains(result) Then
            While UsedNames.Contains(result)
                ofcount += 1
                If ofcount = MaxTries Then
                    Exit While
                End If

                result = ETAList(GRN(ETAList, StrToSeed(result))) & ETBList(GRN(ETBList, StrToSeed(result)))
            End While
        End If

        UsedNames.Add(result)

        Return result
    End Function

    Public Function GetGender(S As Integer) As Gender
        Dim Result As Gender = Gender.Male

        If New Random(S).Next(100000) > 50000 Then
            Result = Gender.Female
        End If

        Return Result
    End Function

    Private Function StrToSeed(input As String) As Integer
        Dim Result As Integer

        For Each l As String In input
            Result += AscW(l)
        Next

        Return Result
    End Function

    Private Function GRN(L As List(Of String), S As Integer) As Integer
        Dim result As Integer
        Dim Max As Integer = L.Count - 1
        Dim t As Integer
        Dim R As Random

        t = (TimeOfDay.Ticks + (Max + S + TimeOfDay.Second)) / 1000000

        R = New Random(t + S)

        result = R.Next((Max) * 10000) / 10000

        Return result
    End Function

    Public Function GetR(Min As Integer, max As Integer) As Integer
        Return nr.Next(Min * 1000, max * 1000) / 1000
    End Function

    Public Function GetTitle(G As Gender, R As Race, IR As Boolean)
        Dim Result As String = ""

        If G = Gender.Male Then
            If R = Race.Human Then
                If IR Then
                    Result = "King"
                Else
                    Result = "Lord"
                End If
            Else
                If R = Race.Elf Then
                    If IR Then
                        Result = "Aran"
                    Else
                        Result = "Heru"
                    End If
                End If
            End If
        Else
            If R = Race.Human Then
                If IR Then
                    Result = "Queen"
                Else
                    Result = "Lady"
                End If
            Else
                If R = Race.Elf Then
                    If IR Then
                        Result = "Tari"
                    Else
                        Result = "Heruin"
                    End If
                End If
            End If
        End If

        Return Result
    End Function
End Module
