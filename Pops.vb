﻿Public Module Pops

    ' S = Island Size (L = 5K-15k, M = 500-4,999, S = 50-499)

    ' City = 800-1000
    ' Town = 200-500
    ' Village = 25-50

    ' Large has City (1-3), Town(10-20), Village(30-50)
    ' Medium has Town(1-4), Village(10-20)
    ' Small has Village (3-8)
    Private Z As Random

    Public Sub InitRan()
        Z = New Random
    End Sub

    Public Enum IslSize
        Large = 0
        Medium = 1
        Small = 2
    End Enum

    Public Enum LocType
        City = 0
        Town = 1
        Village = 2
    End Enum

    Private ReadOnly CitySize As Integer() = {800, 1000}
    Private ReadOnly TownSize As Integer() = {200, 500}
    Private ReadOnly VillageSize As Integer() = {25, 50}
    Private ReadOnly LMaxCity As Integer() = {1, 3}
    Private ReadOnly LMaxTown As Integer() = {10, 17} ' Was 20 -> 17
    Private ReadOnly LMaxVillage As Integer() = {30, 40} ' Was 50 -> 40
    Private ReadOnly MMaxTown As Integer() = {1, 5}
    Private ReadOnly MMaxVillage As Integer() = {10, 20}
    Private ReadOnly SMaxVillage As Integer() = {3, 8}

    Public Function GetPop(L As LocType, S As Integer, Optional GetMax As Boolean = False) As Integer
        Dim Result As Integer
        Dim Ran As Random = New Random(S)
        If Not GetMax Then
            Select Case L
                Case LocType.City
                    Result = Ran.Next(CitySize(0), CitySize(1))
                Case LocType.Town
                    Result = Ran.Next(TownSize(0), TownSize(1))
                Case LocType.Village
                    Result = Ran.Next(VillageSize(0), VillageSize(1))
                Case Else
                    Result = 1
            End Select
        Else
            Select Case L
                Case LocType.City
                    Result = CitySize(1)
                Case LocType.Town
                    Result = TownSize(1)
                Case LocType.Village
                    Result = VillageSize(1)
                Case Else
                    Result = 1
            End Select
        End If
        Return Result
    End Function

    Public Function GetLocs(I As IslSize, Optional GetMax As Boolean = False) As Integer()
        Dim Ran As Random = Z
        Dim C, T, V As Integer
        Dim S As Integer = 10000

        If Not GetMax Then
            Select Case I
                Case IslSize.Large
                    C = Ran.Next(LMaxCity(0) * S, (LMaxCity(1) * S)) / S
                    T = Ran.Next(LMaxTown(0) * S, (LMaxTown(1) * S)) / S
                    V = Ran.Next(LMaxVillage(0) * S, (LMaxVillage(1) * S)) / S
                Case IslSize.Medium
                    T = Ran.Next(MMaxTown(0) * S, (MMaxTown(1) * S)) / S
                    V = Ran.Next(MMaxVillage(0) * S, (MMaxVillage(1) * S)) / S
                Case IslSize.Small
                    V = Ran.Next(SMaxVillage(0) * S, (SMaxVillage(1) * S)) / S
            End Select
        Else
            Select Case I
                Case IslSize.Large
                    C = LMaxCity(1)
                    T = LMaxTown(1)
                    V = LMaxVillage(1)
                Case IslSize.Medium
                    T = MMaxTown(1)
                    V = MMaxVillage(1)
                Case IslSize.Small
                    V = SMaxVillage(1)
            End Select
        End If

        Return New Integer() {C, T, V}
    End Function

End Module
