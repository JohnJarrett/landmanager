﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_prog = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.pb_01 = New System.Windows.Forms.ProgressBar()
        Me.SuspendLayout()
        '
        'lbl_prog
        '
        Me.lbl_prog.AutoSize = True
        Me.lbl_prog.Font = New System.Drawing.Font("Calibri Light", 14.25!)
        Me.lbl_prog.Location = New System.Drawing.Point(12, 10)
        Me.lbl_prog.Name = "lbl_prog"
        Me.lbl_prog.Size = New System.Drawing.Size(76, 23)
        Me.lbl_prog.TabIndex = 1
        Me.lbl_prog.Text = "Progress"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Calibri Light", 14.25!)
        Me.Button1.Location = New System.Drawing.Point(12, 73)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(697, 39)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Doooo Eeeeeeetttttt"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'pb_01
        '
        Me.pb_01.Location = New System.Drawing.Point(12, 36)
        Me.pb_01.Name = "pb_01"
        Me.pb_01.Size = New System.Drawing.Size(694, 31)
        Me.pb_01.TabIndex = 4
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(718, 120)
        Me.Controls.Add(Me.pb_01)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lbl_prog)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl_prog As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents pb_01 As ProgressBar
End Class
