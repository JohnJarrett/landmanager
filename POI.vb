﻿Public Module POI

    Private ListOfPoi As List(Of POIs)

    Public ReadOnly Property APoiT As List(Of POIs)
        Get
            Return ListOfPoi
        End Get
    End Property

    Public Sub MakePoiList()
        Dim tPoi As POIs

        ListOfPoi = New List(Of POIs)

        For Each l As String In IO.File.ReadAllLines("poi.txt")
            tPoi = New POIs(l)
            ListOfPoi.Add(tPoi)
        Next
    End Sub

    Public Structure PPoi
        Public ReadOnly Property Poi As POIs
        Public ReadOnly Property Loc As String

        Public Sub New(P As POIs, L As String)
            Poi = P
            Loc = L
        End Sub

    End Structure

    Public Structure POIs
        Public ReadOnly Property Colour As Color
        Public ReadOnly Property Symb As String
        Public ReadOnly Property Name As String
        Public ReadOnly Property LtoET As Integer

        Private ReadOnly Property Code As String
        'Private ReadOnly Property CID As Integer
        ' Test Repo

        Public ReadOnly Property NextID As String
            Get
                Dim result As String
                '_CID += 1

                result = Code '& CID
                Return result
            End Get
        End Property

        Public Sub New(input As String)
            Colour = ColorTranslator.FromHtml("#" & input.Split(":")(0))
            Symb = input.Split(":")(1)
            Code = input.Split(":")(2)
            Name = input.Split(":")(3)
            LtoET = input.Split(":")(5)
            'CID = 1
        End Sub
    End Structure

    Public Function GetRandPoi() As POIs
        Dim Result As POIs = ListOfPoi(0)
        Dim rn As Integer = GetR(0, 100000)

        For Each pi As POIs In ListOfPoi
            If (rn / 1000) <= pi.LtoET Then
                Result = pi
                Exit For
            End If
        Next

        Return Result
    End Function

    ''' <summary>
    ''' Zero Based
    ''' </summary>
    ''' <param name="ISS"></param>
    ''' <returns></returns>
    Public Function PoiCount(ISS As IslSize) As Integer
        Dim Result As Integer
        Dim d As Double = Double.MaxValue
        Select Case ISS
            Case IslSize.Large
                Result = GetR(100, 200)
            Case IslSize.Medium
                Result = GetR(50, 100)
            Case IslSize.Small
                Result = GetR(5, 25)
            Case Else
                Result = 0
        End Select

        Return Result - 1
    End Function

End Module
