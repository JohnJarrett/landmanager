﻿Public Class Form1

    Private ReadOnly IslandList As New List(Of String)
    Private PMax As Integer

    Const Testing As Boolean = False

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SetForm(Me)
        InitRan()
        MakePoiList()

        DrawMapKey()
    End Sub

    Private Sub DoStuff()
        Dim RRace As Boolean
        Dim S As IslSize
        Dim R As Race
        Dim N As String
        Dim PosLoc As List(Of String)
        Dim c As Integer = 0
        Dim imgp As String

        FillLists()
        ClearLocal()

        If Not Testing Then
            If IO.File.Exists("Islands.txt") Then

                PMax = IO.File.ReadAllLines("Islands.txt").Count * 8

                pb_01.Maximum = PMax

                For Each l As String In IO.File.ReadAllLines("Islands.txt")
                    If Not l = "" Then
                        c += 1
                        PosLoc = New List(Of String)

                        N = l.Split("~")(0)
                        S = l.Split("~")(1)
                        If l.Split("~")(2) = -1 Then
                            RRace = True
                        Else
                            R = l.Split("~")(2)
                        End If

                        imgp = l.Split("~")(3)

                        For Each pl As String In l.Split("~")(4).Split(":")
                            PosLoc.Add(pl)
                        Next

                        MakeIsland(N, S, R, RRace, PosLoc, imgp)
                        ClearLocal()
                        UDateText(c & "/" & IO.File.ReadAllLines("Islands.txt").Count & " : " & N)
                    End If
                Next

                IO.File.WriteAllLines("islanddata.dat", IslandList.ToArray)

                Close()
            End If
        Else
            Test()
        End If
    End Sub

    Public Sub UT(txt As String)
        lbl_prog.Text = txt
        Application.DoEvents()
    End Sub

    Public Sub UpdateProgress()
        pb_01.Value += 1
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        DoStuff()
    End Sub

    Private Sub MakeIsland(IslName As String, S As IslSize, R As Race, RR As Boolean, PLList As List(Of String), oImg As String)
        Dim nIsland As Island
        Dim z As Image

        If RR Then
            nIsland = New Island(IslName, S, True, PLList, Me)
        Else
            nIsland = New Island(IslName, S, False, PLList, Me, R)
        End If

        IslandList.Add(nIsland.SaveData)

        If Not IO.Directory.Exists("maps") Then
            IO.Directory.CreateDirectory("maps")
        End If

        z = GetMap("oMaps\" & oImg, nIsland)

        z.Save("maps\" & IslName & ".png", Imaging.ImageFormat.Png)
    End Sub

    Private Sub Test()
        Dim nIsland As Island
        nIsland = New Island("Test")

        Dim nIv As New ImgTester(GetMap("oMaps\SM0002.png", nIsland))
        nIv.Show()

    End Sub
End Class